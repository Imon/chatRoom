<?php

require_once './registration.php';

$store = new Registration();

if (isset($_POST['submit']) && $_SERVER['REQUEST_METHOD'] == "POST") {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $user_name = $_POST['user_name'];
    $email = $_POST['email'];
    $date_of_birth = $_POST['date_of_birth'];
    $password = $_POST['password'];
    if (!empty($first_name) && !empty($last_name) && !empty($user_name) && !empty($email) && !empty($date_of_birth) && !empty($password)) {
        $store->store($first_name, $last_name, $user_name, $email, $date_of_birth, $password);
        header("location: index.php");
    } else {
        header("location: index.php");
    }
} else {
    echo 'Error.......';
}
//

