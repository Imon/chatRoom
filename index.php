<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Chat Room Registration</title>

        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            h1, h2, h3, h4, h5, h6, p, *{
                padding: 0;
                margin: 0;
                border: 0;
            }
        </style>

    </head>
    <body style="background: #666666">


        <div style="background-color: #333; padding-top: 10px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <p style="color: white">Logo</p>
                    </div>
                    <div class="col-md-6">
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail3"></label>
                                <input  style="height: 26px" type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword3"></label>
                                <input  style="height: 26px" type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
                            </div>
                            <input style="border-radius: 3px;" tabindex="1" id="login-submit" class="login submit-button" type="submit" value="Sign in">

                            <br>
                            <div class="col-md-4 col-md-offset-4">
                                <label>
                                    <a href="" style="color: white">Forgot Password?</a>
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <div style="margin-top: 30px">
                        <div class="well">
                            <?php 
                            $message = "";
                                echo $message;
                            ?>
                            <form method="post" action="store.php"  name="registration">
                                <div class="form-group">
                                    <label for="exampleInputFirstName">First Name</label>
                                    <input type="text" name="first_name" id="firstname" class="form-control" id="exampleInputFirstName" placeholder="First Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputlastName">Last Name</label>
                                    <input type="text" name="last_name" id="lastname" class="form-control" id="exampleInputlastName" placeholder="Last Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUserName">User Name</label>
                                    <input type="text" name="user_name" id="username" class="form-control" id="exampleInputlastName" placeholder="User Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" id="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputDoBirth">Date of Birth</label>
                                    <input type="date" name="date_of_birth" class="form-control" id="exampleInputDoBirth" placeholder="Date Of Birth">
                                </div>
                                
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" name="password" id="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <p style="text-align: center">By clicking Join now, you agree to the our User Agreement, Privacy Policy, and Cookie Policy.</p>
                                <button style="background-color: #f6e312; border-color: #e9ac1a;" type="submit" name="submit" class="btn btn-block">Join Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
        <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        
        <script src="js/form-validation.js"></script>
        <script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
    </body>
</html>